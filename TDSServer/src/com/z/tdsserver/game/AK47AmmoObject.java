package com.z.tdsserver.game;

public class AK47AmmoObject extends GameObject {

	public AK47AmmoObject(int x, int y, int itemID) {
		super(x, y, itemID);
		setObjectDescription("AK 47 Ammo");
	}

}
