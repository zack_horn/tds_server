package com.z.tdsserver.game;

public class AK47RifleWeaponObject extends GameObject {

	public AK47RifleWeaponObject(int x, int y, int itemID) {
		super(x, y, itemID);
		setObjectDescription("AK 47 Rifle");
	}

}
