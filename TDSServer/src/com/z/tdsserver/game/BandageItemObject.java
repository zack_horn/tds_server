package com.z.tdsserver.game;

public class BandageItemObject extends GameObject {

	public BandageItemObject(int x, int y, int itemID) {
		super(x, y, itemID);
		setObjectDescription("Bandage");
	}

}
