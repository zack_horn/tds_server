package com.z.tdsserver.game;

import java.util.ArrayList;
import java.util.Random;

public class Game {
	
	
	private ArrayList<GameObject> gameObjects;
	private ArrayList<Player> playersInGame;
	private int itemID = 0;
	
	private State currentState;
	
	private enum State {
		PRE_GAME_LOBBY, IN_GAME;
	}	

	public Game() {
		gameObjects = createRandomGameObjects();
		playersInGame = new ArrayList<Player>();
	}
	
	public ArrayList<GameObject> createRandomGameObjects() {
		ArrayList<GameObject> gameObjects = new ArrayList<GameObject>();
		Random random = new Random();
		itemID = 0;
		for(int i = 0; i < 100; i++) {
			int randomInt = random.nextInt(3);
			if(randomInt == 0) {				
				gameObjects.add(new AK47RifleWeaponObject(0, 0, itemID));
				itemID++;
				
				gameObjects.add(new AK47AmmoObject(0, 0, itemID));
				itemID++;
			}
			if(randomInt == 1) {				
				gameObjects.add(new PistolWeaponObject(0, 0, itemID));
				itemID++;
				
				gameObjects.add(new PistolAmmoObject(0, 0, itemID));
				itemID++;
			}
			if(randomInt == 2) {
				gameObjects.add(new SniperRifleWeaponObject(0, 0, itemID));
				itemID++;
				
				gameObjects.add(new SniperRifleAmmoObject(0, 0, itemID));
				itemID++;
			}
			if(randomInt == 3) {
				gameObjects.add(new BandageItemObject(0, 0, itemID));
				itemID++;				
			}
		}
		return gameObjects;
	}
	
	public void resetAllGameObjects() {
		gameObjects.removeAll(gameObjects);
		gameObjects = createRandomGameObjects();
	}
	
	public void setCurrentState(State state) {
		currentState = state;
	}
	
	public State getCurrentState() {
		return currentState;
	}
	
	public void addPlayerToGame(Player player) {
		playersInGame.add(player);
	}
	
	public void removePlayerFromGame(Player player) {
		playersInGame.remove(player);
	}
	
}
