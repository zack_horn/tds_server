package com.z.tdsserver.game;

public abstract class GameObject {
	
	private int x, y, itemID;
	private String objectDescription;

	public GameObject(int x, int y, int itemID) {
		this.itemID = itemID;
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getItemID() {
		return itemID;
	}
	
	public void setObjectDescription(String description) {
		objectDescription = description;
	}
	
	public String getObjectDescription() {
		return objectDescription;
	}

}
