package com.z.tdsserver.game;

public class PistolAmmoObject extends GameObject {

	public PistolAmmoObject(int x, int y, int itemID) {
		super(x, y, itemID);
		setObjectDescription("Pistol .45 Ammo");
	}

}
