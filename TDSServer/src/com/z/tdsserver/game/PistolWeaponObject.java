package com.z.tdsserver.game;

public class PistolWeaponObject extends GameObject {	

	public PistolWeaponObject(int x, int y, int itemID) {
		super(x, y, itemID);
		setObjectDescription("Pistol .45");
	}

}
