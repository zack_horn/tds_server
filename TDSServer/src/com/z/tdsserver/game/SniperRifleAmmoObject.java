package com.z.tdsserver.game;

public class SniperRifleAmmoObject extends GameObject {

	public SniperRifleAmmoObject(int x, int y, int itemID) {
		super(x, y, itemID);
		setObjectDescription("Sniper Rifle .308 Ammo");
	}

}
