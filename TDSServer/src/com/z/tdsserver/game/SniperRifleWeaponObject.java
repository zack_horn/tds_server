package com.z.tdsserver.game;

public class SniperRifleWeaponObject extends GameObject {

	public SniperRifleWeaponObject(int x, int y, int itemID) {
		super(x, y, itemID);
		setObjectDescription("Sniper Rifle .308");
	}

}
