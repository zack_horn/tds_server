package com.z.tdsserver.utilities;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import com.z.tdsserver.Server;
import com.z.tdsserver.game.Game;

public class ClientHandler extends Thread {
	
	private Socket socket;
	private String username;
	private State currentState;
	private boolean isRunning = false;
	private Server server;
	private DataInputStream dataInputStream;
	private DataOutputStream dataOutputStream;
	private int packetLength = 0;
	private byte[] buffer;
	private String packetContents, usernameID, protocol, subProtocol;
	private Game game;
	
	
	private enum State {
		WAIT_ON_LOGIN, IDLE, SEARCHING_GAME, PRE_GAME_LOBBY,
		IN_GAME;
	}	
	
	public ClientHandler(Server server, Socket socket) {
		this.socket = socket;
		this.server = server;
		init();
	}
	
	@Override
	public void run() {
		isRunning = true;
		setCurrentState(State.WAIT_ON_LOGIN);
		while(isRunning) {
			switch (getCurrentState()) {
			case WAIT_ON_LOGIN:
				packetLength = getIntFromDataInputStream();
				packetLength = Math.abs(packetLength);
				buffer = new byte[packetLength];
				packetContents = convertPacketBytesToString(buffer); 
				protocol = packetContents.substring(0, packetContents.indexOf(NetworkProtocol.END_MAIN_PROTOCOL_IDENTIFIER));
				subProtocol = packetContents.substring(packetContents.indexOf(NetworkProtocol.END_MAIN_PROTOCOL_IDENTIFIER) + 2, packetContents.indexOf(NetworkProtocol.END_SUB_PROTOCOL_IDENTIFIER));
				usernameID = packetContents.substring(packetContents.indexOf(NetworkProtocol.END_SUB_PROTOCOL_IDENTIFIER) + 2, packetContents.indexOf(NetworkProtocol.END_USERNAME_IDENTIFIER)).trim();
				String passwordID = packetContents.substring(packetContents.indexOf(NetworkProtocol.END_USERNAME_IDENTIFIER) + 3, packetContents.length()).trim();
				// check sql for authentication //
				setCurrentState(State.IDLE);
				
				break;

			case IDLE: 
				packetLength = getIntFromDataInputStream();
				packetLength = Math.abs(packetLength);
				buffer = new byte[packetLength];
				packetContents = convertPacketBytesToString(buffer);
				
				protocol = packetContents.substring(0, packetContents.indexOf(NetworkProtocol.END_MAIN_PROTOCOL_IDENTIFIER));
				subProtocol = packetContents.substring(packetContents.indexOf(NetworkProtocol.END_MAIN_PROTOCOL_IDENTIFIER) + 2, packetContents.indexOf(NetworkProtocol.END_SUB_PROTOCOL_IDENTIFIER));
				
				break;
						
			case SEARCHING_GAME:
				packetLength = getIntFromDataInputStream();
				packetLength = Math.abs(packetLength);
				buffer = new byte[packetLength];
				packetContents = convertPacketBytesToString(buffer);
				
				protocol = packetContents.substring(0, packetContents.indexOf(NetworkProtocol.END_MAIN_PROTOCOL_IDENTIFIER));
				subProtocol = packetContents.substring(packetContents.indexOf(NetworkProtocol.END_MAIN_PROTOCOL_IDENTIFIER) + 2, packetContents.indexOf(NetworkProtocol.END_SUB_PROTOCOL_IDENTIFIER));
				
				
				break;
			
			case IN_GAME:
				packetLength = getIntFromDataInputStream();
				packetLength = Math.abs(packetLength);
				buffer = new byte[packetLength];
				packetContents = convertPacketBytesToString(buffer);
				
				protocol = packetContents.substring(0, packetContents.indexOf(NetworkProtocol.END_MAIN_PROTOCOL_IDENTIFIER));
				subProtocol = packetContents.substring(packetContents.indexOf(NetworkProtocol.END_MAIN_PROTOCOL_IDENTIFIER) + 2, packetContents.indexOf(NetworkProtocol.END_SUB_PROTOCOL_IDENTIFIER));
				
				
				break;
				
			case PRE_GAME_LOBBY:
				packetLength = getIntFromDataInputStream();
				packetLength = Math.abs(packetLength);
				buffer = new byte[packetLength];
				packetContents = convertPacketBytesToString(buffer);
				
				protocol = packetContents.substring(0, packetContents.indexOf(NetworkProtocol.END_MAIN_PROTOCOL_IDENTIFIER));
				subProtocol = packetContents.substring(packetContents.indexOf(NetworkProtocol.END_MAIN_PROTOCOL_IDENTIFIER) + 2, packetContents.indexOf(NetworkProtocol.END_SUB_PROTOCOL_IDENTIFIER));
				
			}	
		}
	}
	
	public void init() {
		try {
			dataInputStream = new DataInputStream(socket.getInputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			dataOutputStream = new DataOutputStream(socket.getOutputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public State getCurrentState() {
		return currentState;
	}
	
	public void setGame(Game game) {
		this.game = game;
	}
	
	public Game getGame() {
		return game;
	}
	
	public void setCurrentState(State state) {
		currentState = state;
	}
	
	public String convertPacketBytesToString(byte[] buffer) {
		try {
			dataInputStream.readFully(buffer, 0, buffer.length);
		} catch (IOException e) {
			isRunning = false;
			e.printStackTrace();
		}
		return new String(buffer);
	}
	
	public Socket getSocket() {
		return socket;
	}
	
	public String getUsername() {
		return username;
	}
	
	public DataOutputStream getDataOutputStream() {
		return dataOutputStream;
	}
	
	public int getIntFromDataInputStream() {
		int result = 0;
		try {
			result = dataInputStream.readInt();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	

}
 