package com.z.tdsserver.utilities;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;

import com.z.tdsserver.Server;

public class ServerInfoPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ServerMessagesEditorPane serverMessagesEditorPane;
	private Server server;

	public ServerInfoPanel(Server server) {
		this.server = server;
		serverMessagesEditorPane = new ServerMessagesEditorPane(server);
		this.setLayout(new BorderLayout());
		this.add(serverMessagesEditorPane, BorderLayout.CENTER);
	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(340, 230);
	}

}
