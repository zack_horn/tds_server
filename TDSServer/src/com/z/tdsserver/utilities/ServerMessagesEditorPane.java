package com.z.tdsserver.utilities;

import java.io.IOException;

import javax.swing.JEditorPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

import com.z.tdsserver.Server;

public class ServerMessagesEditorPane extends JEditorPane {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private HTMLEditorKit htmlEditorKit;
	private HTMLDocument htmlDocument;	
	private Server server;
	
	/////////////////////////////
	private String htmlTag = "<html>";
	private String bodyTag = "<body>";
	private String endHtmlTag = "</html>";
	private String endBodyTag = "</body>";
	private String italicFontTag = "<i>";
	private String italicFontEndTag = "</i>";
	private String imgTag = "<img src =";
	private String endImgTag = "></img>";
	private String colorFontTagRed = "<font color= \"red\">";
	private String colorFontTagGray = "<font color= \"gray\">";
	private String endColorFontTag = "</font>";
	/////////////////////////////////////////////////////

	public ServerMessagesEditorPane(Server server) {
		this.server = server;
		htmlEditorKit = new HTMLEditorKit();
		htmlDocument = new HTMLDocument();
		this.setEditorKit(htmlEditorKit);
		this.setDocument(htmlDocument);
	}
	
	public void appendServerMessage(String string) {
		try {
			htmlEditorKit.insertHTML(htmlDocument, htmlDocument.getLength(), string, 0, 0, null);
		} catch (BadLocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public boolean getScrollableTracksViewportWidth() {
		return true;
	}

}
